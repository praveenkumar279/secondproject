const testObject = require('./object.cjs');

function defaults(testObject,defaultProps){
    if(typeof testObject != 'object' || typeof defaultProps != 'object'){
        return {};
    }
    let objSize = 0;
    for(let key in testObject){
        objSize++;
    }
    for(let key in defaultProps){
        if(key in testObject){
            objSize++;
            testObject[key] = defaultProps[key];
        }
        else{
            objSize++;
            testObject[key] = defaultProps[key];
        }
    }
    
    if(objSize == 0){
        return {};
    }
    return testObject;
}

module.exports = defaults;