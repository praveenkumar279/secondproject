const testObject = require('./object.cjs');

let objSize=0;

function invert(testObject){
    if(typeof testObject != 'object'){
        return {};
    }

    const invertObj = {}
    for(let key in testObject){
        objSize++;
        invertObj[testObject[key]] = key;
    }
    if(objSize==0){
        return {};
    }

    return invertObj;
}

module.exports = invert;