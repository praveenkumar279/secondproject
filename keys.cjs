const testObject = require('./object.cjs');

function keys(testObject){
    if(typeof testObject != 'object'){
        return [];
    }
    const keys = []
    for(let key in testObject){
        keys.push(String(key));
    }
    if(keys.length == 0){
        return [];
    }

    return keys;
}

module.exports = keys;