const testObject = require('./object.cjs');

function values(testObject){
    if(typeof testObject != 'object'){
        return [];
    }
    const Values = [];
    for(let key in testObject){
        Values.push(testObject[key]);
    }
    if(Values.length == 0){
        return [];
    }
    return Values;
}

module.exports = values;