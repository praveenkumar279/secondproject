const testObject = require('./object.cjs');

function pairs(testObject){
    if(typeof testObject != 'object'){
        return [];
    }
    
    const pairsInList = []

    for(let key in testObject){
        pairsInList.push([key,testObject[key]]);
    }
    if(pairsInList.length == 0){
        return [];
    }
    return pairsInList;
}

module.exports = pairs;