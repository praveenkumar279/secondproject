const obj = require('./object.cjs');
const cb = require('./cb.cjs');

function mapObject(obj,value){
    if(typeof obj != 'object'){
        return {};
    }

    let objSize = 0;

    for(let key in obj){
        objSize++;
        obj[key] = cb(obj[key],value);
    }
    if(objSize == 0){
        return {};
    }

    return obj;
}

module.exports = mapObject;